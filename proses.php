<?php
 // Ambil data dari session
 if (isset($_SESSION['nama'])) {
  $nama = $_SESSION['nama'];
 }
 if (isset($_SESSION['kelas'])) {
  $kelas = $_SESSION['kelas'];
 }
 if (isset($_SESSION['nim'])) {
  $nim = $_SESSION['nim'];
 }
 if (isset($_SESSION['email'])) {
  $email = $_SESSION['email'];
 }
 if (isset($_SESSION['alamat'])) {
  $alamat = $_SESSION['alamat'];
 }
 // End ambil data dari session

 // Tambahkan array (hasil dari data session tadi) dengan data inputan yang baru
$nama[] = $_POST['nama'];
$kelas[] = $_POST['kelas'];
$nim[] = $_POST['nim'];
$email[] = $_POST['email'];
$alamat[] = $_POST['alamat'];
 // End script tambah ke array

 // Simpan data array yang baru ke session
 $_SESSION['nama'] = $nama;
 $_SESSION['kelas'] = $kelas;
 $_SESSION['nim'] = $nim;
 $_SESSION['email'] = $email;
 $_SESSION['alamat'] = $alamat;
 // End script simpan ke session
?>

<!DOCTYPE html>
<html>
<head>
  <title>Data hasil form</title>
  <link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>

  <div class="navbar">

  </div>

  <div class="header">
  <h1>Data Hasil Inputan</h1>
  </div>

  <br>
<div class="table">
<table border="1" cellpadding="10" cellspacing="0">
  

  <tr>
    <th>No.</th>
    <th>Nama</th>
    <th>Kelas</th>
    <th>NIM</th>
    <th>E-Mail</th>
    <th>Alamat</th>
  </tr>
<?php
for ($i=0; $i < count($nama); $i++) { 

?>

<tr>
    <td><?= $i + 1; ?> </td>
    <td><?= $nama[$i] ?></td>
    <td><?= $kelas[$i] ?></td>
    <td><?= $nim[$i] ?></td>
    <td><?= $email[$i] ?></td>
    <td><?= $alamat[$i] ?></td>

  </tr>
<?php } ?>
</table>
</div>
</body>
</html>